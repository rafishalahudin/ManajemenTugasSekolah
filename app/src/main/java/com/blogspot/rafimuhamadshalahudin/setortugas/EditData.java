package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.SubMenuBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Calendar;

public class EditData extends AppCompatActivity {
    protected Cursor cursor;
    DBHelper dbcenter;
    EditText nid,ntugas,nmatpel,nguru,ndesk,email;
    Button tomtsng,editsubmit;
    TextView tanggal;
    Spinner jenisedit;
    RatingBar ratingBar;
    Button ton1;
    private Calendar calendar;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_data);
        dbcenter = new DBHelper(this);
        nid = (EditText) findViewById(R.id.EditTextID);
        ntugas = (EditText) findViewById(R.id.editTextNama);
        nmatpel = (EditText) findViewById(R.id.editTextMatpel);
        nguru = (EditText) findViewById(R.id.editTextGuru);
        ndesk = (EditText) findViewById(R.id.editTextDeskripsi);
        tanggal = (TextView) findViewById(R.id.textView3);
        email = (EditText) findViewById(R.id.editalamatemail);
        jenisedit = (Spinner) findViewById(R.id.editjenispengumpulan);
        ratingBar = (RatingBar) findViewById(R.id.editratingBar);
        tomtsng = (Button) findViewById(R.id.edittomtang);
        editsubmit = (Button) findViewById(R.id.btneditsubmit);


        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);


        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_tugas WHERE nama_tugas = '" +
                getIntent().getStringExtra("nama") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            nid.setText(cursor.getString(0));
            ntugas.setText(cursor.getString(1).toString());
            nmatpel.setText(cursor.getString(2).toString());
            nguru.setText(cursor.getString(3).toString());
            ndesk.setText(cursor.getString(4).toString());
            tanggal.setText(cursor.getString(5).toString());
            email.setText(cursor.getString(6).toString());
            for(int i = 0;i<jenisedit.getCount();i++){
                if(jenisedit.getItemAtPosition(0).equals(cursor.getString(7))){
                    jenisedit.setSelection(i);
                }
            }
            ratingBar.setRating(cursor.getFloat(8));


//            jenisedit.(cursor.getString(5).toString());
        }
        ton1 = (Button) findViewById(R.id.btneditsubmit);
        ton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbcenter.getWritableDatabase();
                db.execSQL("update tb_tugas set nama_tugas='"+
                        ntugas.getText().toString() +"', mata_pelajaran='" +
                        nmatpel.getText().toString()+"', nama_guru='"+
                        nguru.getText().toString() +"', desc_tugas='" +
                        ndesk.getText().toString()+"', tgl_pengumpulan='" +
                        tanggal.getText().toString() +"',jenis_pengumpulan_tugas='" +
                        jenisedit.getSelectedItem().toString()+"',alamat_email='" +
                        email.getText().toString()+"',prioritas='" +
                        ratingBar.getRating()+ "' where id_tugas='" +
                        nid.getText().toString()+"'");
                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();

                finish();
            }
        });

    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "Pilih Tangal", Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            showDate(arg1, arg2+1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
//        Button tom = (Button) findViewById(R.id.tomtang);
//        tom.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if (!tom.isPressed()) {
//                    dateView.setText("Tanggal belum dipilih");
//                }
//             else if(tom.isPressed()){
        tanggal.setText(new StringBuilder().append("Tanggal ").append(day).append("/")
                .append(month).append("/").append(year));
    }
}

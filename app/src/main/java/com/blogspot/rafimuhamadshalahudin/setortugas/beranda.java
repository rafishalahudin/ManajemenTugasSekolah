package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class beranda extends AppCompatActivity {

    protected Cursor cursor;
    String[] daftar;
    ArrayList<daftar> array = new ArrayList<>();
    DBHelper dbcenter;
    int row;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


//        ArrayList<daftar> array = new ArrayList<>();

        dbcenter = new DBHelper(this);
        RefreshList();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent i = new Intent(beranda.this,actFrmTugas.class);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_beranda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void RefreshList() {
        array.clear();
        ListView listView = (ListView) findViewById(R.id.list);
        daftarAdapter adapter = new daftarAdapter(this,array);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM tb_tugas", null);
        daftar = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(1).toString();
            daftar lt = new daftar(
                    R.mipmap.ic_launcher,
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getFloat(8));
            array.add(lt);
            adapter.notifyDataSetChanged();
            listView = (ListView) findViewById(R.id.list);
            listView.setAdapter(adapter);
            listView.setSelected(true);
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    return false;


                }
            });
        ((ArrayAdapter) listView.getAdapter()).notifyDataSetInvalidated();

//                cursor.moveToNext();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        RefreshList();
    }
}

        /*daftar s = new daftar(R.mipmap.ic_launcher, "Membuat Tabel","Basis Data","9/8/2017","12.00","dsadsa","dsadsa","dsadsa",(float)3.5);
        adapter.add(s);*/
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,int position,long id){
//                Intent i = new Intent(beranda.this, input.class);
//                i.putExtra("nama_tugas",daftar[position]);
//                i.putExtra("act","view");
//                startActivity(i);
//            }
//        });

        //dummy line

//        daftar d = new daftar(R.mipmap.ic_launcher, "ea", "dua", "tiga", "mpat", "lima", "nam", "tujuh", 3);
//        array.add(d);

        //end of dummy line


        /*Log.wtf("onCreate: ", cursor.getCount()+"");*/

//
//        s = new daftar(R.mipmap.ic_launcher, "Membuat CRUD","Pemrograman Web Dinamis",(float)4.5,"2/9/2017","12.00");
//        adapter.add(s);




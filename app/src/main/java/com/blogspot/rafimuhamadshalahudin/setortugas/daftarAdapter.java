package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rafishalahudin on 10/09/2017.
 */
public class daftarAdapter extends ArrayAdapter<daftar> {


    TextView lblNama;
    TextView lblMatpel;
    TextView namaguru;
    TextView deskripsitugas;
    DBHelper dbcenter;
    Spinner jenis;
    TextView email;
    RatingBar ratein;
    TextView lblTanggal;
    TextView lbljam;


    beranda br;
    public daftarAdapter(Context context, ArrayList<daftar> daftar) {
        super(context,0,daftar);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        final daftar daftar = getItem(position);

        br = new beranda();
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list,parent,false);
        }

//        ImageView imgFoto = (ImageView) convertView.findViewById(R.id.imgFoto);
        lblNama= (TextView) convertView.findViewById(R.id.lblNama);
        lblMatpel = (TextView) convertView.findViewById(R.id.lblMatpel);
        namaguru = (TextView) convertView.findViewById(R.id.namaguru);
        deskripsitugas = (TextView) convertView.findViewById(R.id.deskripsitugas);
        lblTanggal= (TextView) convertView.findViewById(R.id.lblTanggal);
        jenis = (Spinner) convertView.findViewById(R.id.jenispengumpulan);
        email = (TextView)convertView.findViewById(R.id.alamatemail);
        ratein =(RatingBar) convertView.findViewById(R.id.ratein);

        lblNama.setText(daftar.getNama());
        lblMatpel.setText(daftar.getGuru());
//        namaguru.setText(daftar.getGuru());
//        deskripsitugas.setText(daftar.getDeskripsi());
        lblTanggal.setText(daftar.getTanggal());

//        if(daftar.getJenis().equals("Cetak"))
//            jenis.setSelection(0);
//        else if(daftar.getJenis().equals("SoftFile"))
////            jenis.setSelection(1);
//        else if(daftar.getJenis().equals("Email"))
//            jenis.setSelection(2);
//          email.setText(daftar.getEmail());

        ratein.setRating(daftar.getRatingBar());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), input.class);


                i.putExtra("nama", daftar.getNama());
                i.putExtra("matpel", daftar.getMatpel());
                i.putExtra("guru", daftar.getGuru());
                i.putExtra("desk", daftar.getDeskripsi());
                i.putExtra("tanggal",daftar.getTanggal());
                i.putExtra("email",daftar.getEmail());
                i.putExtra("jenis",daftar.getJenis());
                i.putExtra("rate", daftar.getRatingBar());
/*
                i.putExtra("jenis", jenis.getSelectedItem().toString());
                i.putExtra("email", email.getText().toString());
*/
//                i.putExtra("rate", ratein.getRating());

                getContext().startActivity(i);
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final CharSequence[] dialogitem = {"Update Tugas", "Hapus Tugas"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 0:
                                Intent in = new Intent(getContext(), EditData.class);
                                in.putExtra("nama", daftar.getNama());
                                getContext().startActivity(in);
                                break;
                            case 1:
                                dbcenter = new DBHelper(getContext());
                                SQLiteDatabase db = dbcenter.getWritableDatabase();
                                db.execSQL("delete from tb_tugas where nama_tugas = '" + daftar.getNama() + "'");

                                break;
                        }
                    }
                });
                builder.create().show();

                return false;
            }
        });

        return convertView;

    }
}

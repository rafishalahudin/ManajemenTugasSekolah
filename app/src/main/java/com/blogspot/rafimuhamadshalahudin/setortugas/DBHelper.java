package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rafishalahudin on 05/09/2017.
 */
public class DBHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION= 1;
    private static final String DATABASE_NAME = "db_catatan_tugas";
    public DBHelper(Context context){
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TABLE = "CREATE TABLE tb_tugas (\n" +
                "    id_tugas                INT (3)       PRIMARY KEY\n" +
                "                                          NOT NULL,\n" +
                "    nama_tugas              VARCHAR (13),\n" +
                "    mata_pelajaran          VARCHAR (10),\n" +
                "    nama_guru               VARCHAR (15),\n" +
                "    desc_tugas              VARCHAR (200),\n" +
                "    tgl_pengumpulan         DATE,\n" +
                "    jenis_pengumpulan_tugas VARCHAR (25),\n" +
                "    alamat_email            VARCHAR (50),\n" +
                "    prioritas               DOUBLE (2) \n" +
                ");";

        db.execSQL(CREATE_TABLE);
        String INSERT_DATA = "INSERT INTO tb_tugas(\n" +
                "id_tugas,\n" +
                "nama_tugas, \n" +
                "mata_pelajaran, \n" +
                "nama_guru, \n" +
                "desc_tugas, \n" +
                "tgl_pengumpulan, \n" +
                "jenis_pengumpulan_tugas, \n" +
                "alamat_email, \n" +
                "prioritas\n" +
                ")\n"+
                "VALUES (\n" +
                "1,\n" +
                "'Layouting',\n" +
                "'PPB',\n" +
                "'Pa Lukman',\n" +
                "'membuat layout " +
                "untuk terkoneksi dengan database',\n" +
                "'05/09/2017',\n" +
                "'SoftFile',\n" +
                "'@elhakimfirdaus',\n" +
                "5\n" +
                ");";
        db.execSQL(INSERT_DATA);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST tb_tugas");

        onCreate(db);
    }
    public boolean insertData(int id_tugas, String nama_tugas,String mata_pelajaran,String nama_guru,String desc_tugas, String tgl_pengumpulan, String jenis_pengumpulan_tugas,String alamat_email,float prioritas) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id_tugas", id_tugas);
        contentValues.put("nama_tugas", nama_tugas);
        contentValues.put("mata_pelajaran", mata_pelajaran);
        contentValues.put("nama_guru", nama_guru);
        contentValues.put("desc_tugas", desc_tugas);
        contentValues.put("tgl_pengumpulan", tgl_pengumpulan);
        contentValues.put("jenis_pengumpulan_tugas", jenis_pengumpulan_tugas);
        contentValues.put("alamat_email", alamat_email);
        contentValues.put("prioritas", prioritas);
        long result = sqLiteDatabase.insert("tb_tugas", null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
}

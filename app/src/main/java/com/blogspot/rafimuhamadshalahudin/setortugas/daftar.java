package com.blogspot.rafimuhamadshalahudin.setortugas;

/**
 * Created by Rafishalahudin on 10/09/2017.
 */
public class daftar {

//    public daftar(String nama,String matpel,String guru,String deskripsi,String tanggal,String email,String jenis,float RatingBar){
//        this.nama = nama;
//        this.matpel = matpel;
//        this.guru=guru;
//        this.deskripsi = deskripsi;
//        this.tanggal = tanggal;
//        this.email = email;
//        this.jenis = jenis;
//        this.RatingBar = RatingBar;
//    }


    public daftar(Integer fotoID, String nama, String matpel, String guru, String deskripsi, String tanggal, String jenis, String email, float ratingBar) {
        this.fotoID = fotoID;
        this.nama = nama;
        this.matpel = matpel;
        this.guru = guru;
        this.deskripsi = deskripsi;
        this.tanggal = tanggal;
        this.jenis = jenis;
        this.email = email;
        RatingBar = ratingBar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getMatpel() {
        return matpel;
    }

    public void setMatpel(String matpel) {
        this.matpel = matpel;
    }

    public String getGuru() {
        return guru;
    }

    public void setGuru(String guru) {
        this.guru = guru;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getRatingBar() {
        return RatingBar;
    }

    public void setRatingBar(float RatingBar) {
        this.RatingBar = RatingBar;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public Integer getFotoID() {
        return fotoID;
    }

    public void setFotoID(Integer fotoID) {
        this.fotoID = fotoID;
    }

    public Integer fotoID;
    public String nama;
    public String matpel;
    public String guru;
    public String deskripsi;
    public String tanggal;
    public String jenis;
    public String email;
    public float RatingBar;


}

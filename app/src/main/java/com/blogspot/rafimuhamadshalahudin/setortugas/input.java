package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class input extends AppCompatActivity {
    RatingBar ratingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        TextView namatugas = (TextView) findViewById(R.id.namatugas);
        TextView namama = (TextView) findViewById(R.id.namamatpel);
        TextView namagur = (TextView) findViewById(R.id.namaguru);
        TextView deskkk = (TextView) findViewById(R.id.deskr);
        TextView tangg = (TextView) findViewById(R.id.tanggal);
        TextView emai = (TextView) findViewById(R.id.jenispengumpulan);
        ratingBar = (RatingBar) findViewById(R.id.ratein);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        String namatu = b.getString("nama");
        String namamaa = b.getString("matpel");
        String namaguruu = b.getString("guru");
        String krips = b.getString("desk");
        String tan = b.getString("tanggal");
        String em = b.getString("jenis");
        float rate = b.getFloat("rate");

//        Toast.makeText(input.this, namatu, Toast.LENGTH_LONG).show();

        namatugas.setText(namatu);
        namama.setText(namamaa);
        namagur.setText(namaguruu);
        deskkk.setText(krips);
        tangg.setText(tan);
        emai.setText(em);
        ratingBar.setRating(rate);
//        ratingBar.setRating(rate);
    }

    }



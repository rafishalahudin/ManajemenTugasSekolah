package com.blogspot.rafimuhamadshalahudin.setortugas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class actFrmTugas extends AppCompatActivity {
    protected Cursor cursor;
    DBHelper dbcenter;
    Button submit;
    private Calendar calendar;
    private int year, month, day;
    private TextView dateView,tgl_pengumpulan;
    beranda br;
    EditText id_tugas,nama_tugas,mata_pelajaran,nama_guru,desc_tugas,alamat_email;
    Spinner jenis_pengumpulan_tugas;
    RatingBar ratingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_frm_tugas);

        Button bc = (Button) findViewById(R.id.btnsubmit);
        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(actFrmTugas.this, beranda.class);
                startActivity(i);
            }
        });
        br = new beranda();
        dbcenter = new DBHelper(this);
        id_tugas = (EditText) findViewById(R.id.idtugas);
        nama_tugas = (EditText) findViewById(R.id.namatugas);
        mata_pelajaran = (EditText) findViewById(R.id.matapelajaran);
        dateView = (TextView) findViewById(R.id.textView3);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);

        nama_guru = (EditText) findViewById(R.id.namaguru);
        desc_tugas = (EditText) findViewById(R.id.deskripsitugas);
        tgl_pengumpulan = (TextView) findViewById(R.id.textView3);
        jenis_pengumpulan_tugas = (Spinner) findViewById(R.id.jenispengumpulan);
        alamat_email = (EditText) findViewById(R.id.alamatemail);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        submit = (Button) findViewById(R.id.btnsubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                dbcenter.insertData(Integer.parseInt(id_tugas.getText().toString()),
                        nama_tugas.getText().toString(),
                        mata_pelajaran.getText().toString(),
                        nama_guru.getText().toString(),
                        desc_tugas.getText().toString(),
                        tgl_pengumpulan.getText().toString(),
                        jenis_pengumpulan_tugas.getSelectedItem().toString(),
                        alamat_email.getText().toString(),
                        ratingBar.getRating()
                );
                onBackPressed();
                Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
//                br.RefreshList();
            }
        });
    }

        @SuppressWarnings("deprecation")
        public void setDate(View view) {
            showDialog(999);
            Toast.makeText(getApplicationContext(), "Pilih Tangal", Toast.LENGTH_SHORT)
                    .show();
        }

        @Override
        protected Dialog onCreateDialog(int id) {
            // TODO Auto-generated method stub
            if (id == 999) {
                return new DatePickerDialog(this, myDateListener, year, month, day);
            }
            return null;
        }

        private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                showDate(arg1, arg2+1, arg3);
            }
        };

    private void showDate(int year, int month, int day) {
//        Button tom = (Button) findViewById(R.id.tomtang);
//        tom.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if (!tom.isPressed()) {
//                    dateView.setText("Tanggal belum dipilih");
//                }
//             else if(tom.isPressed()){
        dateView.setText(new StringBuilder().append("").append(day).append("/")
                .append(month).append("/").append(year));
    }
    }

